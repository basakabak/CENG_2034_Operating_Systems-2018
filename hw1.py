import os

fout = open("main.txt", "w")
fout.write("created by main process")
fout.write(str(os.getpid()))

forkpid = os.fork()
if forkpid == 0:
    fout = open("child.txt", "w")
    fout.write("created by child process\n")
    cid = str(os.getpid())
    fout.write(cid)
    os.kill(cid)
else:
    fout = open("parent.txt","w")
    fout.write("created by parent process\n")
    paid = str(os.getpid())
    fout.write(paid)

fout.write("\nend of file")
