# CENG 2034: Operating Systems 2018

[![logo](http://www.mu.edu.tr/Icerik/Sayfa/basin.mu.edu.tr/logo_en2.png)](http://wiki.netseclab.mu.edu.tr/index.php?title=CENG_2034:_Operating_Systems)

This git repository contains the lab files to be done in the lessons and some doc. about lectures(mostly further reading).

**Two things are necessary during this lesson.**
*  An operating system with a linux kernel version greater than 2.6. There can be any distro that you will feel comfortable with. (We recommend using  [Ubuntu 16.04 LTS](http://releases.ubuntu.com/16.04/) in a VM) !!! It is your responsibility to use it as the main operating system.
*  A git account to share assignments with us. 

### LAB 01 Syscall:

>"A system call is a way for programs to interact with the operating system. A computer program makes a system call when it makes a request to the operating system's kernel. System calls are used for hardware services, to create or execute a process, and for communicating with kernel services, including application and process scheduling."